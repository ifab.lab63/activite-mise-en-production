![Patch](patch.png)

# Activité de maintenance d’une application et mise en production

> Introduction processus de gestion des correctifs

## Contexte

L'application web [Metabase](https://www.metabase.com) est déployée dans deux environnements séparés "staging" et "production".

L'hébergement s'effectue sur la plateforme Microsoft Azure à l'aide des produits suivants :
- Application web : "Web App Service" (Docker)
- Base de données : "PostgreSQL Flexible Server"

Le processus de provision d'infrastructure et de déploiement applicatif est effectué via une [configuration Terraform](main.tf). Le tout est automatisé par la [plateforme GitLab-CI](.gitlab-ci.yml).

La version actuellement déployée est v0.41.3.1. Cette version est vulnérable à la faille de sécurité "Log4Shell".

**Vous devez établir le plan de remédiation et déployer le correctif en respectant les automatisations en place.**

## Mise en place

Voici un schéma des ressources Azure à provisionner :

![Ressources Azure](azure-resources.png)

### Azure

Afin de provisionner l'infrastructure sur Microsoft Azure avec Terraform :

1. Créer un compte ou se connecter sur le [portail Azure](https://portal.azure.com)

2. Dans la section **Active Directory / App registrations** :
    - Créer une nouvelle application nommée "Terraform" (prendre note des identifiants "Tenant ID" et "Client ID")
    - Créer un nouveau secret client pour l'application dans la section "Certificates and secrets" (prendre note du secret "Secret Value")

3. Dans la section **Subscription / Access control (IAM)** :
    - Assigner le rôle "Owner" à l'application enregistrée

### GitLab

1. Forker (ou cloner puis pousser) ce dépôt GitLab dans votre espace personnel.

2. Dans la section **Settings / CI/CD / Variables**, remplir les variables d'environnement suivantes en tant que masquées et protégées :
   - `ARM_SUBSCRIPTION_ID` : identifiant de souscription Azure
   - `ARM_TENANT_ID` : identifiant tenant de l'application Azure enregistrée
   - `ARM_CLIENT_ID` : identifiant client de l'application Azure enregistrée
   - `ARM_CLIENT_SECRET` : secret client de l'application Azure enregistrée

3. Déclencher le déploiement des environnements staging et production depuis la branche `main` via GitLab-CI (section **CI/CD Pipelines / Run Pipeline**)
    - Etre patient, la provision initiale de l'infrastructure prend du temps
    - Dans les logs, prendre note des URLs des applications déployées dans chaque environnement (de la forme `https://metabase-xxxxxxxx-staging.azurewebsites.net`)

**Notes importantes** :

- Les resources Azure provisionnés rentre dans le cadre du free tiers mensuel (si non consommé)
- Penser à détruire l'infrastructure une fois l'activité terminée pour limiter les coûts (déclencher le job manuel "tf:destroy" dans GitLab-CI)

### Metabase

Pour chaque environnement déployé :

1. Accéder à l'URL de l'application (patienter, la première initialisation est longue)
2. Effectuer le setup initial (utilisateur admin initial)
3. Créer quelques visualisations et/ou tableaux de bords (utiliser les données "Sample").

## Processus

Voici le processus de gestion de correctif attendu à minima :

1. Identifier la version de remédiation sur le site de l'éditeur
2. Identifier l'impact du correctif dans la configuration d'infrastructure as code Terraform
3. Pour chaque environnement, en commençant par staging puis production :
   - Effectuer un backup de la base PostgreSQL
   - Mettre à jour la configuration Terraform de l'environnement par une Merge Request
   - Effectuer la review automatique (CI) et manuelle (code)
   - Merger et déployer l'environnement
   - Vérifier que le patch est effectué et que les visualisations fonctionnent encore dans l'environnement
