terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.38.0"
    }
  }

  backend "http" {
  }
}

provider "azurerm" {
  features {}
}

data "azurerm_subscription" "default" {
}

resource "azurerm_resource_group" "default" {
  name     = "hb-poei-devops"
  location = "West Europe"
}

# ----------------------------------------------------------------------------

resource "random_string" "pg_id" {
  length  = 8
  special = false
  lower   = true
  upper   = false
  numeric = true
}

resource "random_string" "pg_username" {
  length  = 8
  special = false
  lower   = true
  upper   = false
  numeric = false
}

resource "random_password" "pg_password" {
  length  = 32
  special = false
}

resource "azurerm_postgresql_flexible_server" "default" {
  name                   = "hbpoeidevopspostgres${random_string.pg_id.result}"
  location               = azurerm_resource_group.default.location
  resource_group_name    = azurerm_resource_group.default.name
  version                = "14"
  administrator_login    = random_string.pg_username.result
  administrator_password = random_password.pg_password.result
  storage_mb             = 32768
  sku_name               = "B_Standard_B1ms"
  zone                   = "1"
}

resource "azurerm_postgresql_flexible_server_database" "default" {
  for_each = toset(["staging", "production"])

  name      = "metabase-db-${each.value}"
  server_id = azurerm_postgresql_flexible_server.default.id
  collation = "en_US.utf8"
  charset   = "utf8"
}

resource "azurerm_postgresql_flexible_server_firewall_rule" "default" {
  name             = "public-access"
  server_id        = azurerm_postgresql_flexible_server.default.id
  start_ip_address = "0.0.0.0"
  end_ip_address   = "255.255.255.255"
}

locals {
  pg_database_uri_staging    = "postgresql://${random_string.pg_username.result}:${random_password.pg_password.result}@${azurerm_postgresql_flexible_server.default.fqdn}:5432/${azurerm_postgresql_flexible_server_database.default["staging"].name}?sslmode=require"
  pg_database_uri_production = "postgresql://${random_string.pg_username.result}:${random_password.pg_password.result}@${azurerm_postgresql_flexible_server.default.fqdn}:5432/${azurerm_postgresql_flexible_server_database.default["production"].name}?sslmode=require"
}

# ----------------------------------------------------------------------------

resource "random_string" "app_id" {
  length  = 8
  special = false
  lower   = true
  upper   = false
  numeric = true
}

resource "azurerm_service_plan" "default" {
  name                = "app-service-plan"
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  os_type             = "Linux"
  sku_name            = "B1"
}

resource "azurerm_linux_web_app" "default" {
  for_each = {
    "staging" = {
      app_settings = {
        "MB_DB_CONNECTION_URI" = local.pg_database_uri_staging
      }
      app_image_tag = "v0.41.3.1"
    }
    "production" = {
      app_settings = {
        "MB_DB_CONNECTION_URI" = local.pg_database_uri_production
      }
      app_image_tag = "v0.41.3.1"
    }
  }

  name                = "metabase-${random_string.app_id.result}-${each.key}"
  resource_group_name = azurerm_resource_group.default.name
  location            = azurerm_service_plan.default.location
  service_plan_id     = azurerm_service_plan.default.id
  https_only          = true

  site_config {
    always_on          = false
    websockets_enabled = true

    application_stack {
      docker_image     = "metabase/metabase"
      docker_image_tag = each.value.app_image_tag
    }
  }

  app_settings = merge(
    {
      "WEBSITES_PORT" = "3000"
    },
    each.value.app_settings
  )
}

# ----------------------------------------------------------------------------

output "app_url" {
  description = "Application URL"
  value = {
    staging    = "https://${azurerm_linux_web_app.default["staging"].default_hostname}"
    production = "https://${azurerm_linux_web_app.default["production"].default_hostname}"
  }
  sensitive = false
}
